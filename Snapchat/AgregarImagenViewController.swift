//
//  AgregarImagenViewController.swift
//  
//
//  Created by carlos pumayalla on 10/28/21.
//

import UIKit
import FirebaseStorage

class AgregarImagenViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imagen: UIImageView!
    var imagePicker = UIImagePickerController()
    @IBAction func btnSubir(_ sender: Any) {
        let imagenFolder = Storage.storage().reference().child("imagenes")
        let imagenData = imagen.image!.pngData()!
        imagenFolder.child("imagenes.png").putData(imagenData, metadata: nil, completion: {(metadata,error) in
            if error != nil {
                print("ocurrio un error")
            }
        })
    }
    @IBAction func btnImagen(_ sender: Any) {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        present(imagePicker	, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as! UIImage
        imagen.image = image
        imagen.backgroundColor = UIColor.clear
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    

}

//
//  RegistroViewController.swift
//  Snapchat
//
//  Created by carlos pumayalla on 11/9/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegistroViewController: UIViewController {

    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func registrarUsuarioTapped(_ sender: Any) {
        Auth.auth().createUser(withEmail: self.usuarioTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
            print("intentando crear un usuario")
            if error != nil{
                print("se presentó el siguiente error al crear el usuario: \(error)")
                let alerta = UIAlertController(title: "Error al crear usuario", message: "\(error)", preferredStyle: .alert)
                let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
            }else {
                print("El usuario fue creado exitosamente")
                
                Database.database().reference().child("usuarios").child(user!.user.uid).child("email").setValue(user!.user.email)
                
                let alerta = UIAlertController(title: "Creacion de usuario", message: "usuario: \(self.usuarioTextField.text!) se creo correctamente", preferredStyle: .alert)
                let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: { (UIAlertAction) in
                    self.performSegue(withIdentifier: "loginsegue", sender: nil)
                })
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
            }
        })
    }
    
  

}

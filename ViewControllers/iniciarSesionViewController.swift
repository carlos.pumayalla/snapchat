//
//  ViewController.swift
//  Snapchat
//
//  Created by carlos pumayalla on 10/27/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit

class iniciarSesionViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func IniciarSesionTapped(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!){ (user, error) in
            print("intentando iniciar sesion")
            if error != nil{
                print("se presentó el siguiente error: \(error)")
            
                    let alerta = UIAlertController(title: "Registro", message: "usuario: \(self.emailTextField.text!) no está registrado.", preferredStyle: .alert)
                    let btnOK = UIAlertAction(title: "Registrar", style: .default, handler: { (UIAlertAction) in
                    self.performSegue(withIdentifier: "registrosegue", sender: nil)})
                    let btnCancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
                    alerta.addAction(btnOK)
                    alerta.addAction(btnCancel)
                    self.present(alerta, animated: true, completion: nil)
                   
            }else{
                print("inicio de sesion exitoso")
                self.performSegue(withIdentifier: "iniciarsesionsegue", sender: nil)
            }
            
        }
    }
    
    @IBAction func LoginFBTapped(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [.email], viewController: self) { (result) in
            switch result {
            case .success(granted: _, declined: _, token: let token):
                let credential = FacebookAuthProvider.credential(withAccessToken: token!.tokenString)
                Auth.auth().signIn(with: credential) { (result, error) in
                    print("intentando iniciar sesion")
                    if error != nil{
                        print("se presentó el siguiente error: \(error)")
                        
                    }else{
                        print("inicio de sesion con Facebook exitoso")
                    }
                    
                }
            case .cancelled:
                break
            case .failed(_):
                break
            }
            
        }
    }
    @IBAction func registroTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "registrosegue", sender: nil)
    }
}

